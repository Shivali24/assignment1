The Project is to manage StudentData named StudentDataManager
It contains four Apps
All apps contain the same model Student(roll_no,first_name,middle_name,last_name,department)
#app1:AllInformation:Posting and Retrieving and updating  student data to and from the server respectively(uses all function)
#app2:FilterInformation:Filter the queryset based on roll_no(uses filter function)
#app3:ExcludeInformation:Exclude the particular object based on roll_no(uses exclude function)
#app4:getInformation:get a particulaar object(uses get function)