from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse,QueryDict
import json
from AllInformation.models import AllInformation

class AllInformationView(View):
	def get(self,request,*args,**kwargs):  #to retrieve information from the server
		#import pdb; pdb.set_trace()
		query_type=request.GET 
		data = []
		query_set = AllInformation.objects.all()
		for each_object in query_set:
			data.append({
				'roll_no':each_object.roll_no,
				'first_name': each_object.first_name,
				'middle_name':each_object.middle_name,
				'last_name':  each_object.last_name,
				'department':each_object.department,
				})
		return HttpResponse(json.dumps(data),content_type='application/json')

	def post(self,request): # to store information to the server
		query_type=request.POST
		info=AllInformation(roll_no=data.get('roll_no'),first_name=data.get('first_name'),middle_name=data.get('middle_name'),last_name=data.get('last_name'),department=data.get('department'))
		info.save()
		return HttpResponse("Sent")



	def put(self,request):
	 	data=QueryDict(request.body)
	 	q=AllInformation.objects.get(id=data.get('id'))
	 	q.roll_no=data.get('roll_no')
	 	q.department=data.get('department')
	 	q.save()
	 	return HttpResponse("updated!!")





