from django.conf.urls import url
from AllInformation.views import AllInformationView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^all-info/', csrf_exempt(AllInformationView.as_view())),
]