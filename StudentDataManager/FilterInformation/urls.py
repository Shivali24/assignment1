from django.conf.urls import url
from FilterInformation.views import FilterInformationView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^filter-info/', csrf_exempt(FilterInformationView.as_view())),
]