from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
import json
from FilterInformation.models import FilterInformation

class FilterInformationView(View):
	def get(self,request,*args,**kwargs):  #to retrieve information from the server
		#import pdb; pdb.set_trace()
		query_type  = request.GET 
		query_set =[]
		try:
			query_set = FilterInformation.objects.filter(id="2")
		except ObjectDoesNotExist:
			print("Record does not exist!!")

		
		return HttpResponse(query_set)

	def post(self,request): # to store information to the server
		data=request.POST
		info=FilterInformation(roll_no=data.get('roll_no'),first_name=data.get('first_name'),middle_name=data.get('middle_name'),last_name=data.get('last_name'),department=data.get('department'))
		info.save()
		return HttpResponse("Sent")



