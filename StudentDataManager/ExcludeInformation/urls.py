from django.conf.urls import url
from ExcludeInformation.views import ExcludeInformationView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^exclude-info/', csrf_exempt(ExcludeInformationView.as_view())),
]