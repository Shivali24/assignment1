from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
import json
from getInformation.models import GetInformation

class GetInformationView(View):
	def get(self,request,*args,**kwargs):  #to retrieve information from the server
		
		query_type  = request.GET 
		query_set=[]
		try:
			query_set = GetInformation.objects.get(id=query_type.get('id'))
		except ObjectDoesNotExist:
			return HttpResponse("object does not exist")

		return HttpResponse(query_set)

	def post(self,request): # to store information to the server
	    import pdb; pdb.set_trace()
	    data=request.POST
	    info=GetInformation(roll_no=data.get('roll_no'),first_name=data.get('first_name'),middle_name=data.get('middle_name'),last_name=data.get('last_name'),department=data.get('department'))
	    info.save()
	    return HttpResponse("Sent")



	

