from django.conf.urls import url,include
from getInformation.views import GetInformationView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^get-info/', csrf_exempt(GetInformationView.as_view())),
]